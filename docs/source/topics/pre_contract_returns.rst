Pre-Contract Return Handling
============================

- **Pre-Contract Failure Handling:**
  - If a ``pre``contract returns ``False``, or a tuple ``(False, "custom message")``, POP can be configured to raise a ``pop.exc.PreContractFailed`` exception.
  - Returning ``False`` indicates a failure condition, preventing the associated function from being called.
  - A custom message can be provided for more detailed feedback on the failure.

- **Configuration Flag:**
  - The behavior is controlled by the flag ``hub.pop.contract.RAISE_ON_PRE_CONTRACT_FAILURE``.
  - **Default Behavior:** By default, this flag is set to ``False``. POP will not raise an exception for pre-contract failures, maintaining backward compatibility.
  - **Enabling Exception Raising:** Setting this flag to ``True`` activates the new behavio and ``pre`` contract failures will then result in a ``pop.exc.PreContractFailed`` exception.

Usage Example
-------------
Here's an example of a ``pre`` contract for a function named "func" that returns False:

.. code-block:: python

    # project_root/contracts/plugin.py
    def pre_func(hub, ctx):
        if condition_not_met:
            return False


    # project_root/plugin.py
    def func(hub):
        ...

Here's an example of a pre-contract for a function named "func" that returns a custom message:

.. code-block:: python

    # project_root/contracts/plugin.py
    def pre_function(hub, ctx):
        if condition_not_met:
            return False, "Condition for execution not met"


    # project_root/plugin.py
    def function(hub):
        ...

Here is a function that calls "func" and catches the exception:

.. code-block:: python

    import pop.exc


    def main(hub):
        hub.pop.contract.RAISE_ON_PRE_CONTRACT_FAILURE = True

        try:
            hub.plugin.func()
        except pop.exc.PreContractFailed as e:
            # Handle the error as needed
            ...


Configuration
-------------
To enable this behavior, set the ``RAISE_ON_PRE_CONTRACT_FAILURE`` flag to ``True`` in your project:

.. code-block:: python

    # project_root/init.py
    def __init__(hub):
        hub.pop.contract.RAISE_ON_PRE_CONTRACT_FAILURE = True
