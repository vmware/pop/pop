==================
POP Release 22.0.0
==================

Pure Python Subs
----------------

Pure python modules can be added to the hub given a specific name.
In the example below, the datetime module is imported onto the hub as "date".

.. code-block:: python

    import pop.hub

    # Initialize the hub
    hub = pop.hub.Hub()
    # Add a pure python module to the hub
    hub.pop.sub.add(python_import="datetime", subname="date")
    # Access the pure python module via the hub
    assert hub.date.datetime.now()

Config Values in Virtuals
-------------------------

The initial value of hub.OPT is now a ``NullMap``, which always successfully calls ``getitem``.
However, the result of ``getitem`` is ALWAYS another ``NullMap`` until the config has been loaded.
This makes it so that hub.OPT can be used in virtuals safely until the config has been loaded.

Consider the following config file for an app.

.. code-block:: python

    # my_app_root/my_app/conf.py
    CONFIG = {
        "my_opt": {"default": True, "help": "Load my plugin only if this option is True"}
    }
    DYNE = {"my_dyne": ["my_dyne"]}


The following example shows a virtual function that accesses values from ``hub.OPT``.
Before ``hub.pop.config.load("my_dyne")`` is called somewhere in the code, the plugin will fail to load.
Trying to access the plugin or its attributes will result in failure.

After the config has been loaded, the plugin will see the default value of "True" for ``hub.OPT.my_dyne.my_opt``
and the plugin will successfully load.

.. code-block:: python

    # my_app_root/my_app/my_dyne/my_plugin.py


    def __virtual__(hub):
        return hub.OPT["my_dyne"]["my_opt"] is True, "Config prevents loading this module"
