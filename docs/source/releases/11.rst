==============
POP Release 11
==============

We are pleased to release POP 11, this release changes how many
errors are handled.

Specifically, `hub.sub.failed_load` and `hub.sub.doesnt_exist` will
raise AttributeErrors while before they silently returned None.

This makes failures occur sooner and in a more predictable way.

Also, `inspect.signature(hub.sub.mod.func)` will return the
signature of the underlying function.
