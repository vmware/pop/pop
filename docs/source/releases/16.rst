==============
POP 16 Release
==============

POP 16 introduces two powerful extensions to ``__func_alias__`` which are now
fully documented in the :ref:`func_alias` section of the documentation.
