==============
POP 15 Release
==============

This release adds recursive contracts to pop. Recursive contracts work almost
identically to normal contracts with an important distinction - recursive
contracts will be applied to every sub that is loaded under another sub. Note
that for ``call`` contracts, local contracts will take precedence over recursive
contracts.

Given the following structure:

.. code-block::

   ├── README.rst
   ├── fnord
   │   ├── conf.py
   │   ├── fnord
   │   │   ├── contracts
   │   │   ├── do
   │   │   │   └── some.py
   │   │   ├── init.py
   │   │   └── recursive_contracts
   │   │       └── init.py
   │   ├── scripts.py
   │   └── version.py
   ├── pyproject.toml
   ├── requirements.txt
   ├── run.py
   └── setup.py

If your ``recursive_contracts/init.py`` contains

.. code-block:: python

    def pre(hub, ctx):
        print("This happens before eeeeverything")


And ``some.py``:

.. code-block:: python

    def thing(hub):
        print("This is something happening")

And finally, ``run.py``:

.. code-block:: python

    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-

    import pop.hub


    def start():
        hub = pop.hub.Hub()
        hub.pop.sub.add(dyne_name="fnord")
        hub.pop.sub.load_subdirs(sub=hub.fnord, recurse=True)
        hub.fnord.do.some.thing()


    start()

Running this will produce:

.. code-block:: bash

    $ python run.py
    This happens before eeeeverything
    This is something happening
