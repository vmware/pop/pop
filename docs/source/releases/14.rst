==============
POP 14 Release
==============

This release adds ``__sub_virtual__`` function capability to sub `init.py` files.
It works like a ``__virtual__`` function but applies recursively to every
plugin within a sub.

`__init__`  and ``__virtual__`` functions can be asynchronous.

Added typing and docsctrings to everything
