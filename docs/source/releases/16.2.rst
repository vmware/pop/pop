================
POP 16.2 Release
================

POP 16.2 extends the "__getitem__" function of a Sub.

This allows for prettier access of plugins and subs.
For example, Instead of doing:

.. code-block:: python

    getattr(hub.output, hub.OPT.rend.output).display()

You can do:


.. code-block:: python

    hub.output[hub.OPT.rend.output].display()
