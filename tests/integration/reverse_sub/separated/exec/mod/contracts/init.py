def sig_func(hub, *args, **kwargs):
    ...


def pre_func(hub, ctx):
    print("contract-pre")


def call_func(hub, ctx):
    print("contract-pre-call")
    ret = ctx.func(*ctx.args, **ctx.kwargs)
    print("contract-post-call")
    return ret


def post_func(hub, ctx):
    print("contract-post")
    return ctx.ret
