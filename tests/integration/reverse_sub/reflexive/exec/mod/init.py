def __init__(hub):
    # Replace this location on the hub with a dynamic mod
    hub.pop.sub.dynamic(
        subname="mod", sub=hub.exec, resolver=hub.exec.mod.init.resolver
    )


def resolver(hub, ref, context):
    return hub.exec.mod.init.func


def func(hub, *args, **kwargs):
    return args, kwargs
