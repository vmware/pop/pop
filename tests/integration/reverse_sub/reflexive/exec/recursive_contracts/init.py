def sig_func(hub, *args, **kwargs):
    ...


def pre_func(hub, ctx):
    print("recursive-pre")


def call_func(hub, ctx):
    print("recursive-pre-call")
    ret = ctx.func(*ctx.args, **ctx.kwargs)
    print("recursive-post-call")
    return ret


def post_func(hub, ctx):
    print("recursive-post")
    return ctx.ret
