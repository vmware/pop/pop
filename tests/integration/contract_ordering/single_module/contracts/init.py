def pre(hub, ctx):
    print("init-pre")
    ctx.extra = (getattr(ctx, "extra", None) or []) + ["init-pre"]


def post(hub, ctx):
    print("init-post")
    ctx.ret.append("init-post")


def call(hub, ctx):
    print("init-pre-call")
    result = ctx.extra + ["init-pre-call", ctx.func(hub=hub), "init-post-call"]
    print("init-post-call")
    return result


def pre_test_fn(hub, ctx):
    print("init-pre-test-fn")
    ctx.extra = (getattr(ctx, "extra", None) or []) + ["init-pre-test-fn"]


def post_test_fn(hub, ctx):
    print("init-post-test-fn")
    ctx.ret.append("init-post-test-fn")


def call_test_fn(hub, ctx):
    print("init-pre-call-test-fn")
    result = ctx.extra + [
        "init-pre-call-test-fn",
        ctx.func(hub=hub),
        "init-post-call-test-fn",
    ]
    print("init-post-call-test-fn")
    return result
