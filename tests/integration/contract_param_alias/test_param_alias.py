import pytest

import pop.hub


def test_param_alias_str(hub):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="ca")

    assert hub.ca.init.param_alias_str(id="asdf") == "asdf"
    assert hub.ca.init.param_alias_str(id_="asdf") == "asdf"


def test_get_aliases(hub):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="ca")

    assert hub.ca.init.param_alias_str.param_aliases == {"id_": {"id_", "id"}}


def test_param_alias_tuple(hub):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="ca")

    assert hub.ca.init.param_alias_tuple(id="asdf") == "asdf"
    assert hub.ca.init.param_alias_tuple(id_="asdf") == "asdf"


def test_duplicate(hub):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="ca")

    with pytest.raises(TypeError):
        hub.ca.init.param_alias_str(id="asdf", id_="asdf")


def test_multiple_alias(hub):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="ca")

    assert hub.ca.init.param_alias_mult(id="asdf") == "asdf"
    assert hub.ca.init.param_alias_mult(id_="asdf") == "asdf"
    assert hub.ca.init.param_alias_mult(taco="asdf") == "asdf"


def test_multiple_definition(hub):
    hub = pop.hub.Hub()
    with pytest.raises(SyntaxError) as e:
        hub.pop.sub.add("tests.mods.alias_bad")

    assert (
        e.value.args[0]
        == "Multiple aliases or definitions of 'taco' in 'alias_bad.init.param_alias_clash'"
    )


def test_override(hub):
    hub = pop.hub.Hub()
    with pytest.raises(SyntaxError) as e:
        hub.pop.sub.add("tests.mods.alias_clash")

    assert (
        e.value.args[0]
        == "Multiple aliases or definitions of 'id' in 'alias_clash.init.param_multi_def'"
    )
