import pop.hub


def test_when_dyne_sub_is_added_with_recursive_contracts_the_contracts_should_be_applied(
    capsys,
):
    hub = pop.hub.Hub()
    hub.pop.sub.add(pypath="tests.integration.recursive_contract.rc_sub1")

    expected_result = [
        "regular-pre",
        "rc_sub2-recursive-pre",
        "rc_sub1-recursive-pre",
        "regular-pre-call",
        "actual-call",
        "regular-post-call",
        "rc_sub1-recursive-post",
        "rc_sub2-recursive-post",
        "regular-post",
    ]
    hub.pop.sub.load_subdirs(hub.rc_sub1, recurse=True)

    actual_result = hub.rc_sub1.rc_sub2.nested_sub.fnord.test_call()
    assert actual_result == expected_result
