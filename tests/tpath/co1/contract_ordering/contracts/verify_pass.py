"""
A contract that passes its __verify_order__ check
"""


def __verify_order__(hub, contracts: list) -> tuple:
    return True


def pre_acc(hub, ctx):
    hub.co.PRE_ORDER.append(__name__)


def post_acc(hub, ctx):
    ctx.ret.append(__name__)
    return ctx.ret
