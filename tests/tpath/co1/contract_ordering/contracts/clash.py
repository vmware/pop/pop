"""
Contract with a file name that is the same as a recursive contract name
"""


def pre_acc(hub, ctx):
    hub.co.PRE_ORDER.append(__name__)


def post_acc(hub, ctx):
    ctx.ret.append(__name__)
    return ctx.ret
