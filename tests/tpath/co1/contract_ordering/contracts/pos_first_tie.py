"""
A contract with a positive order value that should always be first
"""
__order__ = 1


def __verify_order__(hub, contracts):
    return (
        contracts[0] == __name__,
        f"{__name__} contract must always come first: {contracts}",
    )


def pre_acc(hub, ctx):
    hub.co.PRE_ORDER.append(__name__)


def post_acc(hub, ctx):
    ctx.ret.append(__name__)
    return ctx.ret
