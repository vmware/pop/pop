from pathlib import Path

import pop.hub

current_dir = Path(globals()["__file__"]).parent


def test_repeated_recursive_contracts():
    # A scenario similar to this could occur when recursive contracts
    # were defined on a parent Sub, and a different set of recursive contracts
    # including some of the same contracts were defined on a child Sub.
    hub = pop.hub.Hub()
    hub.pop.sub.add(
        pypath="tests.regression.contract_masking.sub",
        recursive_contracts_static=[
            current_dir / "contract1",
            current_dir / "contract2",
            current_dir / "contract2",
        ],
    )
    val = hub.sub.test.func()
    assert len(val) == 2
    assert val == ["contract2", "contract1"]


def test_regular_and_recursive_contracts():
    # This has not yet happened in the wild to my knowledge
    # but it could happen. Let's make sure it's never a problem.
    hub = pop.hub.Hub()
    hub.pop.sub.add(
        pypath="tests.regression.contract_masking.sub",
        contracts_static=[
            current_dir / "contract1",
            current_dir / "contract2",
        ],
        recursive_contracts_static=[
            current_dir / "contract1",
            current_dir / "contract2",
        ],
    )
    val = hub.sub.test.func()
    assert len(val) == 2
    assert val == ["contract2", "contract1"]
