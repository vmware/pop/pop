def sig_args(hub, *args):
    ...


def sig_kwargs(hub, **kwargs):
    ...


def sig_args_kwargs(hub, *args, **kwargs):
    ...


async def sig_async_func(hub):
    ...
