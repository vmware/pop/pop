import inspect
import unittest

import pytest

import pop.contract as contract
import pop.mods.pop.data as data


class TestImmutableNamespacedMap(unittest.TestCase):
    def test_init(self):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"a": 2, "b": 4}
        inm = data.IMAP(init_dict)
        self.assertEqual(inm, init_dict)

    def test_init_dict(self):
        """
        Verify that a dict is converted into a mutable mapping namespace from self.update
        """
        init_dict = {"k": {}}
        inm = data.IMAP(init_dict)
        self.assertEqual(inm, init_dict)
        self.assertIsInstance(inm["k"], data.IMAP)

    def test_setitem(self):
        inm = data.IMAP({})
        with pytest.raises(TypeError):
            inm["k"] = 1

    def test_delitem(self):
        inm = data.IMAP({})
        with pytest.raises(TypeError):
            del inm["k"]

    def test_getitem(self):
        inm = data.IMAP({"k": "v"})
        self.assertEqual(inm.get("k"), "v")

    def test_getattr(self):
        init_dict = {"k": "v"}
        inm = data.IMAP(init_dict)
        self.assertEqual(inm.k, "v")

    def test_getattr_nested(self):
        d = {"d": "val"}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        inm = data.IMAP(a)

        self.assertEqual(inm.a, b)
        self.assertEqual(inm.a.b, c)
        self.assertEqual(inm.a.b.c, d)
        self.assertEqual(inm.a.b.c.d, "val")

    def test_setattr(self):
        inm = data.IMAP({})
        with pytest.raises(TypeError):
            inm.k = "value"

    def test_overwrite_store(self):
        inm = data.IMAP({})
        with pytest.raises(TypeError):
            inm._store = tuple()

    def test_len(self):
        length = 100
        inm = data.IMAP({f"item_{d}": d for d in range(length)})
        self.assertEqual(len(inm), length)


class TestMutableNamespacedMap(unittest.TestCase):
    def test_init(self):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"1": 2, "3": 4}
        mnm = data.MAP(init_dict)
        self.assertEqual(mnm, init_dict)

    def test_init_dict(self):
        """
        Verify that a dict is converted into a mutable mapping namespace from self.update
        """
        init_dict = {"k": {}}
        mnm = data.MAP(init_dict)
        self.assertEqual(mnm, init_dict)
        self.assertIsInstance(mnm["k"], data.MAP)

    def test_setitem(self):
        mnm = data.MAP()
        mnm["k"] = 1
        self.assertEqual(mnm._store["k"], 1)

    def test_setitem_nested_dict(self):
        mnm = data.MAP()
        value = {"a": {"b": {"c": {}}}}
        mnm["k"] = value
        self.assertEqual(mnm._store["k"], value)
        self.assertIsInstance(mnm._store["k"], data.MAP)
        self.assertIsInstance(mnm._store["k"]["a"], data.MAP)
        self.assertIsInstance(mnm._store["k"]["a"]["b"], data.MAP)
        self.assertIsInstance(mnm._store["k"]["a"]["b"]["c"], data.MAP)

    def test_delitem(self):
        mnm = data.MAP()
        mnm["k"] = "value"
        del mnm["k"]
        self.assertEqual(mnm, {})

    def test_getitem(self):
        mnm = data.MAP({"k": "v"})
        self.assertEqual(mnm.get("k"), "v")

    def test_getattr(self):
        init_dict = {"k": "v"}
        mnm = data.MAP(init_dict)
        self.assertEqual(mnm.k, "v")

    def test_getattr_nested(self):
        d = {"d": {}}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        mnm = data.MAP(a)

        self.assertIsInstance(mnm.a, data.MAP)
        self.assertEqual(mnm.a, b)
        self.assertIsInstance(mnm.a.b, data.MAP)
        self.assertEqual(mnm.a.b, c)
        self.assertIsInstance(mnm.a.b.c, data.MAP)
        self.assertEqual(mnm.a.b.c, d)
        self.assertIsInstance(mnm.a.b.c.d, data.MAP)
        self.assertEqual(mnm.a.b.c.d, {})

    def test_setattr(self):
        mnm = data.MAP()
        mnm.key = "value"
        self.assertEqual(mnm._store["key"], "value")

    def test_setattr_create_nest(self):
        """
        verify that nested namespace values that have never been set get created when they are accessed
        """
        mnm = data.MAP()
        mnm.a.b.c = 1

        self.assertIsInstance(mnm.a, data.MAP)
        self.assertIn("b", mnm.a)
        self.assertIsInstance(mnm.a.b, data.MAP)
        self.assertIn("c", mnm.a.b)
        self.assertEqual(mnm.a.b.c, 1)

    def test_len(self):
        length = 100
        mnm = data.MAP({str(d): d for d in range(length)})
        self.assertEqual(length, len(mnm._store))
        self.assertEqual(length, len(mnm))

    def test_str(self):
        mnm = data.MAP({"a": {}, "b": 1, "c": False, "d": None, "e": lambda: ""})
        self.assertEqual(str(mnm), str(mnm._store))


class TestDynamicMutableNamespacedMap(unittest.TestCase):
    def test_init(self):
        """
        Verify that an init dict is loaded into the namespace
        """
        init_dict = {"1": 2, "3": 4}
        map_ = data.MAP(init_dict)
        self.assertEqual(map_, init_dict)

    def test_init_dict(self):
        """
        Verify that a dict is converted into a mutable mnmping namespace from self.update
        """
        init_dict = {"k": "v"}
        map_ = data.MAP(init_dict)
        self.assertEqual(map_, init_dict)

    def test_setitem(self):
        map_ = data.MAP()
        with pytest.raises(
            AttributeError, match="Cannot store values beginning with '_'"
        ):
            map_._foo = True
        with pytest.raises(
            AttributeError, match="Cannot store values beginning with '_'"
        ):
            map_["_bar"] = True

    def test_setitem_nested_dict(self):
        pass  # This is an integration test

    def test_delattr(self):
        map_ = data.MAP()
        map_.foo = True
        delattr(map_, "foo")
        self.assertNotIn("foo", map_._store)

    def test_delitem(self):
        map_ = data.MAP()
        map_["foo"] = True
        del map_["foo"]
        self.assertNotIn("foo", map_._store)

    def test_get(self):
        map_ = data.MAP()

        self.assertIs(map_.get("foo"), None)
        self.assertIs(map_.get("foo", False), False)

        map_.foo = True
        self.assertIs(map_.get("foo"), True)

    def test_getitem(self):
        map_ = data.MAP({"k": "v"})
        self.assertEqual(map_.get("k"), "v")

    def test_getattr(self):
        init_dict = {"k": "v"}
        map_ = data.MAP(init_dict)
        self.assertEqual(map_.k, "v")

        with pytest.raises(
            AttributeError,
            match=f"'{map_.__class__.__name__}' object has no attribute '_blah'",
        ):
            map_._blah

    def test_getattr_nested(self):
        d = {"d": {}}
        c = {"c": d}
        b = {"b": c}
        a = {"a": b}
        map_ = data.MAP(a)

        self.assertIsInstance(map_.a, data.MAP)
        self.assertEqual(map_.a, b)
        self.assertIsInstance(map_.a.b, data.MAP)
        self.assertEqual(map_.a.b, c)
        self.assertIsInstance(map_.a.b.c, data.MAP)
        self.assertEqual(map_.a.b.c, d)
        self.assertIsInstance(map_.a.b.c.d, data.MAP)
        self.assertEqual(map_.a.b.c.d, {})

    def test_setattr(self):
        pass  # This is an integration test

    def test_setattr_create_nest(self):
        pass  # This is an integration test

    def test_len(self):
        length = 100
        map_ = data.MAP({str(d): d for d in range(length)})
        self.assertEqual(len(map_), len(map_._store), length)

    def test_str(self):
        map_ = data.MAP({"a": {}, "b": 1, "c": False, "d": None, "e": lambda: ""})
        self.assertEqual(str(map_), str(map_._store))

    def test_get_caller(self):
        pass  # This is an integration test

    def test_refresh(self):
        pass  # This is an integration test


class TestUninitializedValue(unittest.TestCase):
    def test_access_error(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        with pytest.raises(
            Exception, match="Access of uninitialized value 'dwrite'"
        ) as e:
            str(dwrite)

    def test_path(self):
        dwrite = data.UninitializedValue(["dwrite"], None)

        dwrite_bar = dwrite.bar

        self.assertIsInstance(dwrite_bar, data.UninitializedValue)
        self.assertEqual(dwrite.bar.__dict__["_path"], ["dwrite", "bar"])

        dwrite_bar_baz = dwrite["bar"].baz
        self.assertIsInstance(dwrite_bar_baz, data.UninitializedValue)
        self.assertEqual(dwrite.bar["baz"].__dict__["_path"], ["dwrite", "bar", "baz"])

    def test_get(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        self.assertIs(dwrite.get("foo"), None)
        self.assertIs(dwrite.get("foo", True), True)

    def test_del(self):
        dwrite = data.UninitializedValue(["dwrite"], None)
        del dwrite["foo"]
        delattr(dwrite, "foo")

    def test_create_on_set(self):
        map_ = data.MAP()
        map_.foo.bar.baz = True
        self.assertIs(map_.foo.bar.baz, True)

        map_.o.bar["baz"] = True
        self.assertIs(map_.o.bar.baz, True)


class TestOwnerWriteableMapping(unittest.TestCase):
    def test_setitem(self):
        g = data.OwnerWriteableMapping()

        def assign(h):
            g.i1 = "val1"
            g.i2 = "val2"

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        i1 = g._store["i1"]
        self.assertEqual(i1.val, "val1")
        i2 = g._store["i2"]
        self.assertEqual(i2.val, "val2")

        self.assertEqual(i1.owner, cassign)
        self.assertEqual(i2.owner, cassign)

    def test_setitem_nested(self):
        g = data.OwnerWriteableMapping()

        def assign(h):
            g.a.b1 = "item"
            g.w.x = {"y": {"z": {}}}

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        self.assertEqual(g.a.b1, "item")
        node = g
        for i in ("w", "x", "y", "z"):
            self.assertIsInstance(node, g.__class__)
            node = node[i]

    def test_writelock(self):
        g = data.OwnerWriteableMapping()

        def assign1(h):
            g.lineno = inspect.currentframe().f_lineno

        cassign1 = contract.Contracted(None, None, assign1, None, "assign1")
        cassign1()

        with pytest.raises(
            Exception,
            match=f"'lineno' was previously assigned by 'assign1' (.*.py:{g.lineno})",
        ):
            g.lineno = False

        def assign2(h):
            g.lineno = False

        cassign2 = contract.Contracted(None, None, assign2, None, "assign2")

        self.assertIs(g._store["lineno"].owner, cassign1)
        with pytest.raises(
            Exception,
            match=f"'lineno' was previously assigned by 'assign1' (.*.py:{g.lineno})",
        ):
            cassign2()

        self.assertIsInstance(g.lineno, int)

    def test_nested_assignment(self):
        g = data.OwnerWriteableMapping()

        def nest_assign():
            g.foo = True

        def assign(h):
            g.report_line = inspect.currentframe().f_lineno + 1
            nest_assign()

        cassign = contract.Contracted(None, None, assign, None, "assign")
        cassign()

        self.assertIs(g._store["report_line"].owner, cassign)
        with pytest.raises(
            Exception,
            match=f"'foo' was previously assigned by 'assign' (.*.py:{g.report_line})",
        ):
            g.foo = False

        self.assertIs(g.foo, True)

    def test_doesntexist(self):
        g = data.OwnerWriteableMapping()

        with pytest.raises(
            Exception, match="Access of uninitialized value 'doesntexist'"
        ):
            str(g.doesntexist)

    def test_str(self):
        d = {"a": {"b": {"c": 1}}}
        g = data.OwnerWriteableMapping(d)

        self.assertEqual(str(g), str(d))
        self.assertEqual(g, d)
