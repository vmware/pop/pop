from pop.dirs import UniqueDirList
from pop.dirs import UniqueList


def test_uniquelist_init():
    l = UniqueList([0, 1, 0])
    assert l == [0, 1]


def test_uniquelist_append():
    l = UniqueList([0, 1])
    l.append(0)
    assert l == [0, 1]


def test_uniquelist_insert():
    l = UniqueList([0, 1])
    l.insert(0, 0)
    assert l == [0, 1]


def test_uniquelist_extend():
    l = UniqueList()
    l.extend([0, 1, 0])
    assert l == [0, 1]


def test_uinquelist_addition():
    l = UniqueList([0, 1])
    assert l + [0] == [0, 1]
    # the result is not a UniqueList, not much we can do about that.
    assert [0] + l == [0, 0, 1]


def test_uniquelist_subtraction():
    initial = [0, 1, 2, 3]
    l = UniqueList(initial)

    assert l - [3, 1] == [0, 2]

    # original is un-changed
    assert l == initial


def test_uniquedirlist_paths():
    dirname = "imaginary"
    l = UniqueDirList([dirname, f"{dirname}/../{dirname}"])
    assert len(l) == 1
    assert l[0].startswith(dirname), f"{l[0]} doesn't start with {dirname}"
