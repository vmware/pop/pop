import pytest

import pop.hub
from pop.contract import _order


@pytest.fixture(scope="function")
def hub():
    yield pop.hub.Hub()


def _format_refs(refs: list):
    # Split each ref by their common base
    return [r.split("contract_ordering.")[1] for r in refs]


def test_base(hub):
    """
    Verify the deterministic ordering of a complex contract system
    """
    hub.pop.sub.add(dyne_name="co")

    # Run the "acc" function which has a bunch of "post" contracts that add their ref to a growing list
    ret = hub.co.init.acc()

    pre_order = _format_refs(hub.co.PRE_ORDER)
    post_order = _format_refs(ret)

    assert pre_order == [
        # This one should always come first
        "contracts.pos_first",
        # This one should always come second
        "contracts.pos",
        # These two have the same order so they should come in the positive order section in the default order
        "contracts.dup2",
        "contracts.dup1",
        # Unordered contracts, starting with implicitly assigned init
        "contracts.init",
        # Unordered contracts in teh same order as in the __contracts__ list
        "contracts.clash",
        "contracts.unordered_2",
        "contracts.unordered_1",
        "contracts.verify_pass",
        # Unordered recursive contracts, starting with implicitly assigned init
        "recursive_contracts.init",
        # Unordered recursive contracts in teh same order as in the __contracts__ list
        "recursive_contracts.clash",
        "recursive_contracts.unordered_4",
        "recursive_contracts.unordered_3",
        # Negatively ordered contract that should come second to last
        "contracts.neg",
        # Negatively ordered contract that should always come last
        "contracts.neg_last",
    ]

    # Verify stacky contracts
    assert post_order == [
        r for r in reversed(pre_order)
    ], "post contracts are not in the reverse order of pre contracts"


def test_recursive(hub):
    """
    Verify the deterministic ordering of a simple recursive contract system
    """
    hub.pop.sub.add(dyne_name="co")
    hub.pop.sub.load_subdirs(hub.co, recurse=True)

    # Run the "acc" function which has a bunch of "post" contracts that add their ref to a growing list
    ret = hub.co.nest.init.acc()

    pre_order = _format_refs(hub.co.PRE_ORDER)
    post_order = _format_refs(ret)

    assert post_order == [
        # Recursive unordered contracts, starting with implicitly assigned init
        "recursive_contracts.unordered_3",
        "recursive_contracts.unordered_4",
        "recursive_contracts.clash",
        "recursive_contracts.init",
    ]

    # Verify stacky contracts
    assert pre_order == [
        r for r in reversed(post_order)
    ], "post contracts are not in the reverse order of pre contracts"


def test_verify_fail(hub):
    """
    Ensure that a mod with a "__verify_order__" function that returns False throws an error
    """
    hub.pop.sub.add(dyne_name="co")
    with pytest.raises(
        ValueError, match=r"'verify_fail' failed to verify its order: A comment"
    ):
        _order([hub.co._contract_subs[0].verify_fail])


def test_verify_raise(hub):
    """
    Ensure that a mod with a "__verify_order__" function that raises an error gets run
    """
    hub.pop.sub.add(dyne_name="co")
    with pytest.raises(ValueError, match="Failed order verification"):
        _order([hub.co._contract_subs[0].verify_raise])


def test_zero(hub):
    """
    Verify that invalid __order__ numbers raise an error
    """
    hub.pop.sub.add(dyne_name="co")
    with pytest.raises(
        ValueError,
        match="Contracts can only have an __order__ with a absolute value greater than or equal to 1",
    ):
        _order([hub.co._contract_subs[0].zero])


def test_invalid(hub):
    """
    Verify that invalid __order__ values raise an error
    """
    hub.pop.sub.add(dyne_name="co")
    with pytest.raises(
        ValueError, match=r"invalid literal for int\(\) with base 10: 'taco'"
    ):
        _order([hub.co._contract_subs[0].invalid])


def test_dupe_warning(hub):
    """
    Verify that duplicate orders throw a warning
    """
    hub.pop.sub.add(dyne_name="co")
    with pytest.warns(
        SyntaxWarning, match=r"Multiple contracts share an order of '100': dup1, dup2"
    ):
        _order([hub.co._contract_subs[0].dup1, hub.co._contract_subs[0].dup2])


def test_conditional_fail(hub):
    """
    If two contracts __verify_order__ that they should be first, then throw an error.
    """
    hub.pop.sub.add(dyne_name="co")

    with pytest.raises(ValueError, match="'pos_first_tie' failed to verify its order:"):
        _order(
            [hub.co._contract_subs[0].pos_first, hub.co._contract_subs[0].pos_first_tie]
        )
