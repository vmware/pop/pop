import pprint
import sys
import unittest.mock as mock

import pytest

import pop.exc
import pop.hub


def test_config(hub):
    with mock.patch.object(sys, "argv", ["program"]):
        hub.pop.config.load("config", "config")

    pprint.pprint(hub.OPT)


def test_test_opt_virtual():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="v1")

    # Verify that opt is empty
    assert not hub.OPT.v1.load

    # Verify that module is not loaded
    with pytest.raises(pop.exc.PopError):
        assert hub.v1.load.A != 1
        assert hub.v1.load.a() == 1

    # Load the config, which should cause the module to pass
    hub.pop.config.load("v1", "v1", parse_cli=False)

    # Verify that OPT is now populated
    assert hub.OPT.v1.load is True

    # Verify that the module was able to load
    assert hub.v1.load.A == 1
    assert hub.v1.load.a() == 1
