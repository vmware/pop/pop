import pytest

import pop.exc
import pop.hub


@pytest.fixture(scope="function")
def hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="cn")
    hub.pop.contract.RAISE_ON_PRE_CONTRACT_FAILURE = False
    yield hub


def test_pre_fail_sync(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        hub.cn.test.func1()


def test_pre_fail_sync_with_message(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        hub.cn.test.func2()


async def test_pre_fail_async(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        await hub.cn.test.afunc1()


async def test_pre_fail_async_with_message(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        await hub.cn.test.afunc2()


def test_pre_fail_gen(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        for _ in hub.cn.test.gen1():
            ...


def test_pre_fail_gen_with_message(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        for _ in hub.cn.test.gen2():
            ...


async def test_pre_fail_agen(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        async for _ in hub.cn.test.agen1():
            ...


async def test_pre_fail_agen_with_message(hub):
    with pytest.raises(Exception, match="Pre Contract did not fail"):
        async for _ in hub.cn.test.agen2():
            ...
